<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/5/2018
 * Time: 6:24 PM
 */

use frameworkphp\Router;



//default routes
Router::add('^admin$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^admin/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$',[ 'prefix' => 'admin']);

Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');