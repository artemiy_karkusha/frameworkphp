<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/5/2018
 * Time: 12:21 PM
 */

define("DEBUG",1);
define("ROOT",dirname(__DIR__));
define("WWW",ROOT . '/public');
define("APP",ROOT . '/app');
define("CORE",ROOT . '/vendor/frameworkphp/core');
define("LIBS",ROOT . '/vendor/frameworkphp/core/libs');
define("CACHE",ROOT . '/tmp/cache');
define("CONFIG",ROOT . '/config');
define("LAYOUT",'watches');

// http://frameworkphp.loc/public/index.php
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
// http://frameworkphp.loc/public/
$app_path = preg_replace("#[^/]+$#", '', $app_path);
// http://frameworkphp.loc/
$app_path = str_replace("/public/",'',$app_path);
define("PATH",$app_path);
define("ADMIN",PATH . '/admin');

//Подключаем скрипт что бы не подключать классы по отдельности
require_once ROOT . '/vendor/autoload.php';
