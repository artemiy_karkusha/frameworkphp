<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/7/2018
 * Time: 11:04 AM
 */

namespace app\controllers;

use frameworkphp\App;
use frameworkphp\Cache;

class MainController extends AppController {

    public function indexAction(){
        //echo __METHOD__ ;
        $posts = \R::findAll('test');
        //$post = \R::findOne('test','id =?',[2]);
        //debug($post);
        $this->setMeta(App::$app->getProperty('shop_name'), 'Описание','Ключи');
        $name = 'Andrey';
        $age = 30;
        $names = ['Jonh', 'Jane', 'Make'];
        $cache = Cache::instance();
        $data = $cache->get('test');
        if(!$data){
            $cache->set('test',$names);
        }
        //$data = $cache->get('test');
        //debug($data);
        $this->set(compact('name','age','names','posts'));
    }
}