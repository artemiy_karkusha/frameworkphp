<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/7/2018
 * Time: 12:12 PM
 */

namespace app\controllers;

use app\models\AppModel;
use frameworkphp\base\Controller;

class AppController extends  Controller {

    public function __construct($route)
    {
        parent::__construct($route);
        new AppModel();
    }

}