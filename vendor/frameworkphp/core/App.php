<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/5/2018
 * Time: 1:17 PM
 */

namespace frameworkphp;

class App
{
    public static $app;

    public function __construct()
    {
        session_start();
        $query = trim($_SERVER['QUERY_STRING'], '/');
        self::$app = Registry::instance();
        $this->getParams();
        new ErrorHandler();
        Router::dispatch($query);
    }

    protected function getParams(){
        $params = require_once CONFIG . '/params.php';
        if(!empty($params)){
            foreach ($params as $k => $v){
                self::$app->setProperty($k,$v);
            }
        }
    }
}