<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/7/2018
 * Time: 1:55 PM
 */

namespace frameworkphp;

class Db{

    use TSingletone;

    protected function __construct(){
        $db = require_once CONFIG . '/db.php';
        class_alias('\RedBeanPHP\R','\R');
        \R::setup($db['dsn'],$db['user'],$db['pass']);
        if( !\R::testConnection()){
            throw new \Exception("Нет соединение с БД", 500);
        }
        \R::freeze(true);
        if (DEBUG){
            \R::debug(true,1);
        }
    }

}