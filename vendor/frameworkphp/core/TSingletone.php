<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/5/2018
 * Time: 1:49 PM
 */

namespace frameworkphp;


trait TSingletone{

    private static $instance;

    public static function instance(){
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

}