<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 7/7/2018
 * Time: 1:47 PM
 */

namespace frameworkphp\base;


use frameworkphp\Db;

abstract class Model{

    public $attributes = [];
    public $errors = [];
    public $rules = [];

    public function __construct(){
        Db::instance();
    }

}